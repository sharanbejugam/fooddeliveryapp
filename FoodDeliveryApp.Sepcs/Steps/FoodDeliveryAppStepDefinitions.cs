﻿using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace FoodDeliveryApp.Sepcs.Steps
{
    [Binding]
    public sealed class StepDefinition1
    {
        private readonly ScenarioContext _scenarioContext;
        readonly RestarurantManager restarurantManager = new RestarurantManager();
        public StepDefinition1(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"the user is customer")]
        public void GivenTheUserIsCustomer()
        {
        }

        [Given(@"the menu have some items in it")]
        public void GivenTheMenuHaveSomeItemsInIt(Table table)
        {
            var i = 0;
            var menuKeys = restarurantManager.Menu.Keys;
            var menuValues = restarurantManager.Menu.Values;
            foreach (var row in table.Rows)
            {
                
                if(row[0] != menuKeys[i] || row[1] != menuValues[i].ToString())
                {
                    throw new Exception("Did not matched");
                }
                i++;
            }
        }

        [When(@"the customer enters any '(.*)' to order")]
        public void WhenTheCustomerEntersAnyToOrder(string item_name)
        {
            ThenTheShouldToAddedToTheOrdersList(item_name);
        }

        [Then(@"the '(.*)' should to added to the orders list")]
        public void ThenTheShouldToAddedToTheOrdersList(string item_name)
        {
            restarurantManager.TotalOrders.Add(item_name);
        }

        [Then(@"this should be displayed ""(.*)""")]
        public void ThenThisShouldBeDisplayed(string item)
        {
            if (restarurantManager.UpdateTotalOrders(item) != $"Please select an item present in the menu to order. {item} is not available.") throw new Exception("Test Failed. Item is present in the menu.");
        }


        [When(@"the menu is empty")]
        public void WhenTheMenuIsEmpty()
        {
        }

        [Then(@"this output should be diplayed ""(.*)""")]
        public void ThenThisOutputShouldBeDiplayed(string p0)
        {
            if (restarurantManager.OrderItem() != p0) throw new Exception("Test Failed. Menu is not empty");
        }

        [Given(@"the user is restarurant manager")]
        public void GivenTheUserIsRestarurantManager()
        {
        }

        [When(@"the orders list is empty")]
        public void WhenTheOrdersListIsEmpty()
        {
        }

        [Then(@"this output should be displayed ""(.*)""")]
        public void ThenThisOutputShouldBeDisplayed(string p0)
        {
            if (restarurantManager.GetLastFiveOrders() != p0) throw new Exception("Test Failed. orders list not empty.") ;
        }

        [When(@"the orders list is not empty")]
        public void WhenTheOrdersListIsNotEmpty()
        {
        }

        [Then(@"a list of last five orders should be displayed")]
        public void ThenAListOfLastFiveOrdersShouldBeDisplayed(Table table)
        {
            var i = 0;
            if(table.RowCount != restarurantManager.TotalOrders.Count()) throw new Exception("Did not matched");
            foreach (var row in table.Rows)
            {
                if(i < restarurantManager.TotalOrders.Count() && row[0] != restarurantManager.TotalOrders[i++])
                {
                    throw new Exception("Did not matched");
                }
            }
        }

        [BeforeScenario("mytag","wrongitem")]
        private void Fillmenu()
        {
            restarurantManager.Menu.Add("Chapati", 100);
            restarurantManager.Menu.Add("dosa", 100);
        }

        [BeforeScenario("lastfiveorders")]
        private void Fillorders()
        {
            restarurantManager.TotalOrders.Add("Chapati");
            restarurantManager.TotalOrders.Add("puri");
            restarurantManager.TotalOrders.Add("dosa");
            restarurantManager.TotalOrders.Add("idli");
            restarurantManager.TotalOrders.Add("wada");
        }
    }
}
