﻿Feature: Food Delivery App
	Simple Food Delivery Console App that have two types of users, one is coustomer and other is Restaurant Manager

@mytag
Scenario: Placing an order when the menu is not empty 
	Given the user is customer
	And the menu have some items in it
	| item_name | item_price |
	| Chapati   | 100        |
	| dosa      | 100        |
	When the customer enters any '<item_name>' to order
	Then the '<item_name>' should to added to the orders list
@wrongitem
Scenario: Placing an order when customer selects an item which is not in menu
	Given the user is customer
	When the customer enters any '<item_name>' to order
	Then this should be displayed "Please select an item present in the menu to order. '<item_name>' is not available."

Scenario: Placing an order when the menu is empty 
	Given the user is customer
	When the menu is empty
	Then this output should be diplayed "There are no items to order!"

Scenario: Running query to get last 5 orders when the orders list is empty 
	Given the user is restarurant manager
	When the orders list is empty
	Then this output should be displayed "There are no orders to show."

@lastfiveorders
Scenario: Running query to get last 5 orders when the orders list is not empty 
	Given the user is restarurant manager
	When the orders list is not empty
	Then a list of last five orders should be displayed
	| item name |
	| Chapati   |
	| puri      |
	| dosa      |
	| idli      |
	| wada      |