﻿Feature: Food Delivery App
	Simple Food Delivery Console App that have two types of users, one is coustomer and other is Restaurant Manager

@mytag
Scenario: Placing an order when there is atleast one item in menu 
	Given there is atleast one item in menu
	When the coustomer orders the item
	Then the order should be executed and that order should be added to the Total Orders

Scenario: Placing an order when the menu is empty 
	Given the menu is empty 
	When the coustomer orders the item
	Then the output should be diplayed "There are no items to order!"

Scenario: Running query Get last 5 orders when the orders list is empty 
	Given the orders list is empty
	When the restaurant manager runs the query
	Then this output should be diplayed "There are no orders to show now!"

Scenario: Running query Get last 5 orders when the orders list is not empty 
	Given the orders list is not empty
	When the restaurant manager runs the query
	Then this output should be diplayed:
	Examples: 
	| item name |
	| Chapati   |
	| puri      |
	| dosa      |