﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;

namespace FoodDeliveryApp
{
    public class RestarurantManager
    {
        public SortedList<string,int> Menu = new SortedList<string,int>();
        private Dictionary<string, int> TotalOrdersFrequency { get; set; } = new Dictionary<string, int>();
        private Dictionary<string, int> Itemwiserevenue { get; set; } = new Dictionary<string, int>();
        public List<string> TotalOrders = new List<string>();

        public void AddItem(string item,int value)
        {
            Menu.Add(item,value);
            TotalOrdersFrequency.Add(item, 0);
            Itemwiserevenue.Add(item, 0);
        }

        public string GetLastFiveOrders()
        {
            TotalOrders.Reverse();
            var totalOrders = TotalOrders;
            TotalOrders.Reverse();
            if (totalOrders.Count() > 0)
            {
                int limit = Math.Min(5, TotalOrders.Count());
                string output = "";
                for(int i=0;i<limit;i++)
                {
                    output += totalOrders[i];
                    if (i != limit - 1) output += "\n";
                }
                return output;
            }
            return "There are no orders to show.";
        }

        public string GetTheMostPopularItem()
        {
            return TotalOrdersFrequency.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
        }

        public string GetItemWithHighestRevenue()
        {
            Itemwiserevenue.OrderBy(value => value.Value);
            return Itemwiserevenue.ElementAt(Itemwiserevenue.Count() - 1).Key;
        }

        public String[] GetItemsBelowThePrice(int price)
        {
            List<string> ans = new List<string>();
            foreach(var val in Menu)
            {
                if (val.Value < price) ans.Add(val.Key);
            }
            return ans.ToArray();
        }

        public string UpdateTotalOrders(string item)
        {
            if (!Menu.ContainsKey(item)) return $"Please select an item present in the menu to order. {item} is not available.";
            TotalOrders.Add(item);
            TotalOrdersFrequency[item]++;
            Itemwiserevenue[item] = TotalOrdersFrequency[item] * Menu[item];
            return "Order placed succesfully";
        }

        public string OrderItem()
        {
            if (Menu.Count() > 0)
            {
                Console.WriteLine("Select an item name to place order:");
                var i = 0;
                foreach (var item in Menu)
                {
                    Console.WriteLine(i + ") " + item.Key + " - " + item.Value);
                }
                var itemchoice = Console.ReadLine();
                return UpdateTotalOrders(itemchoice);
            }
            return "There are no items to order!";
        }
    }
}
