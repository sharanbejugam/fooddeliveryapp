﻿using System;
using System.Linq;

namespace FoodDeliveryApp
{
    public class Program
    {   
        static void Main(string[] args)
        {
            var restarurantmanager = new RestarurantManager();
            while(true)
            {
                Console.WriteLine("Select a User type(Or close the application to exit): \n\t1) Customer \n\t2) Restarurant Manager");
                string choice = Console.ReadLine();
                switch(choice)
                {
                    case "1":
                        Console.WriteLine(restarurantmanager.OrderItem());
                            break;
                    case "2":
                        {
                            Console.WriteLine(
                                "1) Add item to menu\n"+
                                "2) Get last 5 orders\n"+
                                "3) Get the most popular item\n"+
                                "4) Get the item with highest revenue\n"+
                                "5) Get items below the price\n");
                            var itemchoice = Console.ReadLine();
                            switch(itemchoice)
                            {
                                case "1":
                                    {
                                        Console.WriteLine("Enter the item name: ");
                                        var itemname = Console.ReadLine();
                                        Console.WriteLine("Enter the item price: ");
                                        var itemprice = int.Parse(Console.ReadLine());
                                        restarurantmanager.AddItem(itemname, itemprice);
                                        break;
                                    }
                                case "2":
                                    {
                                        Console.WriteLine(restarurantmanager.GetLastFiveOrders());
                                        break;
                                    }
                                case "3":
                                    {
                                        Console.WriteLine(restarurantmanager.GetTheMostPopularItem());
                                        break;
                                    }
                                case "4":
                                    {
                                        Console.WriteLine(restarurantmanager.GetItemWithHighestRevenue());
                                        break;
                                    }
                                case "5":
                                    {
                                        Console.WriteLine("Enter a price to get all the items below that price: ");
                                        var price = int.Parse(Console.ReadLine());
                                        Console.WriteLine(restarurantmanager.GetItemsBelowThePrice(price));
                                        break;
                                    }
                            }
                            break;
                        }
                }
            }
        }
    }
}
